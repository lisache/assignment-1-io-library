%define SYS_WRITE 1   ; номер системного вызова write
%define FD_STDOUT 1   ; дескриптор stdout
%define SYS_READ 0   ; номер системного вызова read
%define END_SYMB 0   ; символ окончания строки
%define FD_STDIN 0   ; дескриптор stdin
section .text
 

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; в rdi - указатель на нуль-терминированную строку
string_length:
      xor rax, rax
    .count:
      cmp byte [rdi+rax], END_SYMB ; проверка на признак окончания строки (символ с кодом 0)
      je .end               ; если символы в строке закончились, переход к .end
      inc rax               ; иначе увеличиваем счетчик и переходим к следующей итерации
      jmp .count
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; в rdi - указатель на нуль-терминированную строку
print_string:
    xor rax, rax
    push rdi     ; поместить в стек указатель на строку, сохранить caller-saved регистр
    call string_length
    push rax     ; поместить в стек длину строки
    mov  rax, SYS_WRITE  ; номер системного вызова write
    mov  rdi, FD_STDOUT ; дескриптор stdout
    pop  rdx     ; количество байт для записи
    pop  rsi     ; адрес строки
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi     ; поместить код символа на вершину стека 
    mov rax, SYS_WRITE   ; номер системного вызова write
    mov rdi, FD_STDOUT   ; дескриптор stdout
    mov rsi, rsp ; rsp указывает на вершину стека, в котором находится код символа, помещаем указатель в rsi
    mov rdx, 1   ; количество байт для записи
    syscall
    pop rdi
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; в rdi - беззнаковое 8-байтовое число
print_uint:
    xor rcx, rcx
    mov rax, rdi     ; поместить беззнаковое 8-байтовое число в rax
    mov rdi, 10      ; в rdi поместить 10 для дальнейшей операции деления
    dec rsp
    mov byte[rsp], END_SYMB
    .loop:
	xor rdx, rdx 
	inc rcx      ; счетчик увеличить на 1
	div rdi      ; разделить значение в rax на 10, в результате в rdx запишется остаток от деления, в rax - частное
	add rdx, 48  ; перевод цифры в ASCII код
	dec rsp      ; место в стеке для цифры числа
	mov byte[rsp], dl
	test rax, rax  ; проверка на наличие цифр в числе
	jnz .loop    ; если число не прочитано до конца, переход к следующей итерации
    xor rdi, rdi     ; очистка rdi для дальнейшей записи в него цифр числа из стека
    .loop_print:
	mov rdi, rsp  ; в rdi адрес первого символа
        push rcx  
	call print_string
	pop rcx
	add rsp, rcx   ; восстановить rsp
	inc rsp
	ret 


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:	
    cmp rdi, 0      ; проверка знака числа
    jge print_uint  ; если число >=0, вывести его
    push rdi        ; сохранить caller-saved регистр
    mov rdi, 45     ; записать в rdi "-"
    call print_char ; вывести "-" 
    pop rdi
    neg rdi         ; получить отрицательное значение
    jmp print_uint ; вывести число

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; в rdi - указатель на 1 строку, в rsi - на 2 строку
string_equals:
    push rdi            ; сохранить значения rdi и rsi в стек (функция string_length изменит их) 
    push rsi
    call string_length  ; в rax будет записана длина первой строки
    pop rsi
    pop rdi
    mov rdx, rax        ; записать в rdx длину строки
    push rdi            ; сохранить значения rdi и rsi в стек (функция string_length изменит их) 
    push rsi
    mov rdi, rsi
    call string_length  ; в rax будет записана длина второй строки
    pop rsi
    pop rdi
    mov rcx, rax        ; в rcx - длина второй строки, в rdx - первой
    cmp  rdx, rcx       ; если длины строк не совпадают, переход к .fail (возвращает 0)
    jnz .fail
    test rcx, rcx       ; если обе строки нулевой длины (они равны), возвращаем 1
    jz .ok 
    .loop:
        dec rcx
        mov al, byte[rdi]  ; в младший байт rax помещаем символ из первой строки
        cmp al, byte[rsi]  ; сравнение символов
        jnz .fail          ; если не равны, переход к .fail
        inc rsi            ; переход к следующему символу
        inc rdi
        test rcx, rcx      ; проверка на конец цикла
        jnz .loop
   .ok:
       mov rax, 1
       ret
   .fail:
       mov rax, 0
       ret
   

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0        ; выделить место в стеке
    mov rax, SYS_READ    ; номер системного вызова read
    mov rdi, FD_STDIN    ; stdin дескриптор
    mov rsi, rsp  ; сохранить в rsi адрес символа
    mov rdx, 1
    syscall
    pop rax       ; возвратить символ
    ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; в rdi - адрес начала буфера, в rsi - размер буфера
read_word:
    mov r8, rdi        ; поместить в регистр r8 адрес начала буфера
    mov r9, rsi        ; поместить в r9 размер буфера
    dec r9             ; уменьшить размер буфера для дальнейшего размещения символа окончания строки	
    xor rcx, rcx       ; очистка счетчика цикла
    .check1:
	push rcx       ; сохранить caller-saved регистры
        push r8
        push r9
        call read_char ; прочитать символ
	pop r9
        pop r8       
        pop rcx
        cmp rcx, r9    ; если слово достигло размера буфера, переход к .fail
        je .fail
        
    .checker:
        cmp al, 0x20  ; проверка на пробел
        je .continue  
        cmp al, 0x9 ; проверка на табуляцию
        je .continue
        cmp al, 0xA; проверка на перевод строки
        je .continue
        cmp rax, 0 ; проверка на конец ввода
        je .end

    .write:
        mov byte [r8+rcx], al      ; положить символ в буфер
        inc rcx               ; посимвольный сдвиг
        jmp .check1
    .continue:
	cmp rcx, 0
	je .check1
	jmp .end
    .fail:
        xor rax, rax
        ret
    .end:  
        mov byte[r8+rcx], END_SYMB   ; записать в конец символ окончания строки
        mov rdx, rcx          ; в rdx записать длину слова
	mov rax, r8           ; в rax записать адрес слова
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; в rdi - указатель на строку
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor rdx, rdx
    xor rcx, rcx
    mov r8, 10                   ; записать в r8 10 для дальнейшей операции умножения
    .loop:
        cmp byte[rdi+rcx], 48    ; сравнить, если код символа меньше 48 ("0" в кодировке ASCII), переход к .end
        jb .end
	cmp byte[rdi+rcx], 58    ; сравнить, если код символа больше или равна 58 (больше "9" в кодировке ASCII), переход к .end
	jnb .end
	mov sil, byte[rdi+rcx]   ; иначе поместить в младший байт rsi код символа
	sub rsi, 48              ; перевести в десятичный формат 
	mul r8                   ; умножить rax на 10 (для смещения цифр единиц)
	add rax, rsi             ; прибавить к числу в rax новое число
	inc rcx                  ; увеличить счетчик цикла для перехода к новому символу
	jmp .loop
    .end:
	mov rdx, rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; в rdi - указатель на строку
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte [rdi], 45   ; проверка знака числа (символ с кодом 45 "-")
    jne parse_uint       ; если число положительное, переходим к его возвращению в rax
    inc rdi              ; сдвиг rdi на 1 (из-за символа "-" в начале)
    call parse_uint      ; возвращаем число в rax, в фукнции parse_uint проверяется также отсутствие пробельных символов
    test rdx, rdx        ; если число прочитать не удалось, переход к .fail
    je .fail
    inc rdx              ; увеличить длину на один (для включения в длину знака отрицания)
    neg rax              ; получить отрицательное значение
    ret
    .fail:
	xor rax, rax
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - указатель на строку, rsi - указатель на буфер, rdx - длина буфера
string_copy:
    xor rax, rax
    xor rcx, rcx
    push rdi
    push rsi
    push rdx
    call string_length    ; если строка не умещается в буфер, переход к .fail
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jnbe .fail
    
    xor rax, rax
    .loop:
	cmp byte [rdi+rcx], END_SYMB      ; проверка на символ окончания строки
        je .end
	mov al, byte [rdi+rcx]     ; записать в младший байт rax очередной символ из строки
        mov byte [rsi + rcx], al   ; записать в буфер
        inc rcx
	jmp .loop
	
    .fail:
	xor rax, rax
        ret
    .end:
        mov byte [rsi + rcx], END_SYMB    ; записать символ окончания строки
	mov rax, rdx
        ret
